/**
 * Created by pohchye on 30/6/2016.
 */

var express = require("express");
var app = express();

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));
app.use(function (req, res) {
    console.info("File not found");
    res.redirect("error.html");
});

app.set("port",
    process.argv[2] || process.env.APP_PORT || 3004
);
app.listen(app.get("port"), function () {
    console.info("Day04, Express Server started on port %s", app.get("port"));
});

